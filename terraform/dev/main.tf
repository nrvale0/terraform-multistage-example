module "myinfra-common" {
  source = "https://git.private.example.org/terraform-myinfra-common"
  estate_stage = "dev"
}

terraform {
  backend "atlas" {
    name = "myinfra/state/dev"
    ...
  }
}
