module "myinfra-common" {
  source = "https://git.private.example.org/terraform-myinfra-common"
  estate_stage = "prod"
}

terraform {
  backend "atlas" {
    name = "myinfra/state/prod"
    ...
  }
}
